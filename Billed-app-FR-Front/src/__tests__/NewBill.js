/**
 * @jest-environment jsdom
 */

import { fireEvent, screen, waitFor } from "@testing-library/dom"
import NewBillUI from "../views/NewBillUI.js"
import NewBill from "../containers/NewBill.js"
import '@testing-library/jest-dom'
import '@testing-library/user-event'
import userEvent from "@testing-library/user-event"
import e from "express"
import { ROUTES } from "../constants/routes.js"


describe("Given I am connected as an employee", () => {
  describe("When I am on NewBill Page", () => {
    test("Then the message error should not display", () => {
     window.onNavigate = (pathname) => {
        document.body.innerHTML = ROUTES({ pathname });
      }
      window.localStorage.setItem('user', JSON.stringify({
        type: 'Employee'
      }))

      
      const html = NewBillUI()
      document.body.innerHTML = html
      //to-do write assertion
       const newInstance = new NewBill ({
         document,
         onNavigate,
         firestore: null,
         localStorage: window.localStorage
       })

      const inputFile = screen.getByTestId('file')
      const hdlChangeFile = jest.fn(newInstance.handleChangeFile)
      inputFile.addEventListener('change', hdlChangeFile)
      fireEvent.change(inputFile)
      expect(hdlChangeFile).toHaveBeenCalled()
      expect(inputFile.files[0]).not.toBeNull()       
    })
  })
})

describe("I am connected as an employee", () => {
  describe("I am on NewBill Page", () => {
    describe ("The form should be Posed", () => {

      window.onNavigate = (pathname) => {
        document.body.innerHTML = ROUTES({ pathname });
      }
      window.localStorage.setItem('user', JSON.stringify({
        type: 'Employee'
      }))
      
      const html = NewBillUI()      
      document.body.innerHTML = html
      
      
      test("then the expenive type should be filled ",()=> {
        const expensiveType = screen.getByTestId('expense-type')        
        expect(expensiveType).toHaveValue()
      })

      test("then the expenive Name should be filled ",()=> {
        const expensiveName = screen.getByTestId('expense-name')
        expect(expensiveName).not.toBeNull()
      })


      test("then the amount should be filled ",()=> {
        const amount = screen.getByTestId('amount')
        expect(amount).not.toBeNull()
      })

      test("then the Date should be filled ",()=> {
        const date = screen.getByTestId('datepicker')
        expect(date).not.toBeNull()
      })

      test("then the TVA should be filled ",()=> {
        const vat = screen.getByTestId('vat')
        expect(vat).not.toBeNull()
      })


      test("then the percentage should be filled ",()=> {
        const pct = screen.getByTestId('pct')
        expect(pct).not.toBeNull()
      })

      test("then the commentary should be filled ",()=> {
        const commentary = screen.getByTestId('commentary')
        expect(commentary).not.toBeNull()
      })
     
      test("then the handle submit should be called", async()=> {        
        
        const onNavigate = (pathname) => {
          document.body.innerHTML = ROUTES({ pathname })
        }

        const newInstance = new NewBill ({
          document,
          onNavigate,
          fireStore: null,
          localStorage: window.localStorage
        })
        await waitFor(() => screen.getByTestId("form-new-bill"));
        const btnSubmit = screen.getByTestId('form-new-bill')
        const hdlSubmit = jest.fn(e => newInstance.handleSubmit(e))
        btnSubmit.addEventListener('click', hdlSubmit)
        fireEvent.click(btnSubmit)
        expect(hdlSubmit).toHaveBeenCalled()
                
      })                 
    })
  })
})

