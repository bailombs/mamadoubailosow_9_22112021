/**
 * @jest-environment jsdom
 */
import '@testing-library/jest-dom'
import {screen, waitFor} from "@testing-library/dom"
import BillsUI from "../views/BillsUI.js"
import { bills } from "../fixtures/bills.js"
import { ROUTES_PATH} from "../constants/routes.js";
import {localStorageMock} from "../__mocks__/localStorage.js";
import userEvent from '@testing-library/user-event';

import { ROUTES } from '../constants/routes.js';
import router from "../app/Router.js";
import  Bills from "../containers/Bills.js"
import  mockStore from "../__mocks__/store.js"




describe("Given I am connected as an employee", () => {
  describe("When I am on Bills Page", () => {
    test("Then bill icon in vertical layout should be highlighted", async () => {

      Object.defineProperty(window, 'localStorage', { value: localStorageMock })
      window.localStorage.setItem('user', JSON.stringify({
        type: 'Employee'
      }))
      const root = document.createElement("div")
      root.setAttribute("id", "root")
      document.body.append(root)
      router()
      window.onNavigate(ROUTES_PATH.Bills)
      await waitFor(() => screen.getByTestId('icon-window'))
      const windowIcon = screen.getByTestId('icon-window')
      //to-do write expect expression
      expect(windowIcon).toHaveClass('active-icon')
    })   
    test("Then bills should be ordered from earliest to latest", () => {
      document.body.innerHTML = BillsUI({ data: bills })
      const dates = screen.getAllByText(/^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$/i).map(a => a.innerHTML)
      const antiChrono = (a, b) => ((a < b) ? 1 : -1)
      const datesSorted = [...dates].sort(antiChrono)
      expect(dates).toEqual(datesSorted)
    })
  })
})


describe('When I am on bills pages and its loading', () => {
  test('Then I should see loading page', () => {
    const html = BillsUI({ loading: true });
    document.body.innerHTML = html;
    expect(screen.getAllByText('Loading...')).toBeTruthy();
  });

  test('Then I should see error page', () => {
    const html = BillsUI({ error: true });
    document.body.innerHTML = html;
    expect(screen.getAllByText('Erreur')).toBeTruthy();
  });
}); 


//teste de  l'appel de la fonction handleClickNewBill et l'envoie de note  frais 

describe('When I am on Bills Page and I click on the New bill button ', () => {
     
  test("Then, it should render New bill page ", async () => {
    window.onNavigate = (pathname) => {
      document.body.innerHTML = ROUTES({ pathname });
    };
    document.body.innerHTML = BillsUI({ data: bills });

    const newInstance = new Bills({
      document,
      onNavigate,
      firestore: null,
      localStorage: window.localStorage,
    });

    await waitFor(() => screen.getByTestId("btn-new-bill"));
    const btnBill = screen.getByTestId("btn-new-bill");
    // mocker le pointer 
    const hdlClickNewBill = jest.fn(newInstance.handleClickNewBill);

    //evenement listener Bill
    btnBill.addEventListener("click", hdlClickNewBill);
    // Lancer l'evenement 
    userEvent.click(btnBill);

    expect(hdlClickNewBill).toHaveBeenCalled();
    expect(screen.getAllByText('Envoyer une note de frais')).toBeTruthy();
    expect(screen.getByTestId("form-new-bill")).not.toBe(null);
  });    
})


describe('When I am on Bills Page and I click on an eye icon in bill row', () => {

  it("Then, a modal should be open", () => {       

   //afficher les bills
   document.body.innerHTML = BillsUI({ data: bills});
   
   $.fn.modal = jest.fn(); // simulation d'une modale

   const iconEyes = screen.getAllByTestId('icon-eye')
      
   // simulation du click pour handClickIconEye

    const newInstance = new Bills(
     { 
       document, 
       onNavigate, 
       firestore: null, 
       localStorage: window.localStorage 
     }            
   )      
   
    const iconEye = iconEyes[0]
    const hdlClickIconEye = jest.fn(newInstance.handleClickIconEye(iconEye))        
    iconEye.addEventListener('click', hdlClickIconEye) 

    userEvent.click(iconEye)  
    expect(hdlClickIconEye).toHaveBeenCalled();    
    expect(screen.getByTestId('modaleFile')).toBeTruthy()  
  })
})


// Test d'integartion  des bills avec GET
describe("Given I am a user connected as Employee", () => {
  jest.mock("../app/Store", ()=> mockStore)
  describe("When I navigate to Bills UI", () => {
    
    test("fetches bills from mock API GET", async () => {
      jest.spyOn(mockStore, "bills")         
      localStorage.setItem("user", JSON.stringify({ type: "employee", email: "a@a" }));
      const root = document.createElement("div")
      root.setAttribute("id", "root")
      document.body.append(root)
      router()
      window.onNavigate(ROUTES_PATH.Bills)

      // recuperer  les bills
      const bills = await mockStore.bills();
      await waitFor(()=> screen.getByTestId("btn-new-bill"))
      const btnNewBill = screen.getByTestId("btn-new-bill")
      expect(btnNewBill.textContent).toBe("Nouvelle note de frais")

       // recuperation table des bills
      await waitFor(()=> screen.getByTestId("tbody"))      
      const tableBills = screen.getByTestId("tbody")      
      expect(tableBills).not.toBeNull()

      expect(jest.spyOn(mockStore, "bills")).toHaveBeenCalledTimes(1);
      //  le nombre de bills doit etre 4
      expect((await bills.list()).length).toBe(4);
    });

    test("fetches bills from an API and fails with 404 message error", async () => {
      mockStore.bills.mockImplementationOnce(() =>
        Promise.reject(new Error("Erreur 404"))
      );

      // interface avec un error code
      const html = BillsUI({
        error: "Erreur 404"
      });
      document.body.innerHTML = html;

      const message = await screen.getByText(/Erreur 404/);
      //  le message error message 400
      expect(message).toBeTruthy();
    });

    test("fetches messages from an API and fails with 500 message error", async () => {
      mockStore.bills.mockImplementationOnce(() =>
        Promise.reject(new Error("Erreur 500"))
      );
     
      const html = BillsUI({
        error: "Erreur 500"
      });
      document.body.innerHTML = html;

      const message = await screen.getByText(/Erreur 500/);
      // le message  error 400
      expect(message).toBeTruthy();
    });
  });
});



